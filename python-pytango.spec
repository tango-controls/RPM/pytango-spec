# Original file created by pyp2rpm-3.3.8

%global pypi_name pytango
%global pypi_version 9.4.2

%define debug_package %{nil}

Name:           python-%{pypi_name}
Version:        %{pypi_version}
Release:        1%{?dist}.maxlab
Summary:        A python binding for the Tango control system

License:        LGPL
URL:            http://gitlab.com/tango-controls/pytango
Source0:        https://pypi.io/packages/source/p/%{pypi_name}/%{pypi_name}-%{pypi_version}.tar.gz

BuildRequires: boost-devel
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: libtango9-devel >= 9.4.1, libtango9-devel < 9.5.0
BuildRequires: cppzmq-devel

Requires:      libtango9 >= 9.4.1, libtango9 < 9.5.0
Requires:      zeromq

%description
Python binding for Tango, a library dedicated to distributed control systems.
PyTango exposes the complete Tango C++ API through the tango python module.
It also adds a bit of abstraction by taking advantage of
the Python capabilites.

%package -n     python3-%{pypi_name}
Summary:        %{summary}

BuildRequires:  python3-devel
BuildRequires:  boost-python3-devel
BuildRequires:  python3dist(numpy)
BuildRequires:  python3dist(setuptools)

%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(numpy)
Requires:       python3dist(packaging)
Requires:       python3dist(psutil)
Requires:       boost-python3

%description -n python3-%{pypi_name}
Python binding for Tango, a library dedicated to distributed control systems.
PyTango exposes the complete Tango C++ API through the tango python module.
It also adds a bit of abstraction by taking advantage of
the Python capabilites.

%prep
%autosetup -n %{pypi_name}-%{pypi_version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info
# Fix interpreter directives
# Explicitely pass tango/databaseds/DataBaseds as only .py files are checked by default
pathfix.py -pn -i %{__python3} tango tango/databaseds/DataBaseds

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%license LICENSE.txt
%doc README.rst
%{python3_sitearch}/__pycache__/PyTango*
%{python3_sitearch}/PyTango.py
%{python3_sitearch}/tango
%{python3_sitearch}/%{pypi_name}-%{pypi_version}-py%{python3_version}.egg-info

%changelog
* Fri Jul 28 2023 Benjamin Bertrand - 9.4.2-1
- Version 9.4.2
* Tue Oct 18 2022 Benjamin Bertrand - 9.3.6-1
- Version 9.3.6
* Thu Mar 24 2022 Benjamin Bertrand - 9.3.3-1
- Initial package.
